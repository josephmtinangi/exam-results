<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('wallet', 'HomeController@wallet')->name('wallet');

Route::group(['middleware' => 'auth'], function () {

	Route::get('results', 'ResultController@index')->name('results.all');
	
	Route::resource('uploads/{hash}/results', 'ResultController');
	
	Route::resource('uploads', 'UploadController');

	Route::post('uploads/{hash}/results/export', 'ResultController@export')->name('results.export');
	Route::resource('payments', 'PaymentController');

	Route::resource('charges', 'ChargeController');

	Route::get('settings', 'SettingsController@index')->name('settings.index');
	Route::post('settings/grades', 'SettingsController@updateGrades')->name('settings.grades.update');
	Route::post('settings/aggt', 'SettingsController@updateAggt')->name('settings.aggt.update');

	Route::get('settings/profile', 'SettingsController@showProfile')->name('profile.show');
	Route::post('settings/profile', 'SettingsController@updateProfile')->name('profile.update');
	Route::get('settings/account', 'SettingsController@showAccount')->name('account.show');
	Route::post('settings/account', 'SettingsController@updateAccount')->name('account.update');
});

Route::post('download-sample', 'UploadController@downloadSample')->name('download-sample');