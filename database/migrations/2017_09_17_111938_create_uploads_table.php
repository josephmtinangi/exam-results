<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->double('user_id')->unsigned();
            $table->string('hash');
            $table->string('student_name');
            $table->double('civ')->unsigned();
            $table->double('hist')->unsigned();
            $table->double('geo')->unsigned();
            $table->double('kisw')->unsigned();
            $table->double('engl')->unsigned();
            $table->double('phy')->unsigned();
            $table->double('chem')->unsigned();
            $table->double('bio')->unsigned();
            $table->double('bmath')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
