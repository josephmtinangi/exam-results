<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->double('user_id')->unsigned();
            $table->string('upload_hash');
            $table->string('student_name');
            $table->double('aggt')->unsigned();
            $table->string('division');
            $table->double('civ')->unsigned();
            $table->string('civ_grade');
            $table->double('hist')->unsigned();
            $table->string('hist_grade');
            $table->double('geo')->unsigned();
            $table->string('geo_grade');
            $table->double('kisw')->unsigned();
            $table->string('kisw_grade');
            $table->double('engl')->unsigned();
            $table->string('engl_grade');
            $table->double('phy')->unsigned();
            $table->string('phy_grade');
            $table->double('chem')->unsigned();
            $table->string('chem_grade');
            $table->double('bio')->unsigned();
            $table->string('bio_grade');
            $table->double('bmath')->unsigned();            
            $table->string('bmath_grade');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
