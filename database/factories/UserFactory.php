<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'settings' => [
        	'grades' => [
        		['from' => 81, 'to' => 100, 'label' => 'A', 'value' => 1],
        		['from' => 61, 'to' => 80, 'label' => 'B', 'value' => 2],
        		['from' => 41, 'to' => 60, 'label' => 'C', 'value' => 3],
        		['from' => 31, 'to' => 40, 'label' => 'D', 'value' => 4],
        		['from' => 21, 'to' => 30, 'label' => 'E', 'value' => 5],
        		['from' => 0, 'to' => 20, 'label' => 'F', 'value' => 6],
        	],
        	'aggt' => [
        		['from' => 7, 'to' => 14, 'value' => 'I'],
        		['from' => 15, 'to' => 21, 'value' => 'II'],
        		['from' => 22, 'to' => 25, 'value' => 'III'],
        		['from' => 26, 'to' => 28, 'value' => 'IV'],
        		['from' => 29, 'to' => 45, 'value' => 'O'],
        	],
        ],
        'remember_token' => str_random(10),
    ];
});
