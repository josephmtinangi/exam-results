<?php

use Faker\Generator as Faker;

$factory->define(App\Charge::class, function (Faker $faker) {
    return [
        'user_id' => function () {
        	return factory('App\User')->create()->id;
        },
        'upload_hash' => uniqid(true),
        'candidates' => $faker->numberBetween(25, 150),
        'per_candidate' => 100,
        'total' => $faker->numberBetween(25, 150) * 100,        
    ];
});
