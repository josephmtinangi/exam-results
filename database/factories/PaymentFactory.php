<?php

use Faker\Generator as Faker;

$factory->define(App\Payment::class, function (Faker $faker) {
    return [
        'user_id' => function () {
        	return factory('App\User')->create()->id;
        },
        'amount' => $faker->randomNumber,
    ];
});
