<?php

use Faker\Generator as Faker;

$factory->define(App\Upload::class, function (Faker $faker) {
    
    $hash = uniqid(true);
    
    return [
        'user_id' => function () {
        	return factory('App\User')->create()->id;
        },
        'hash' => $hash,
        'student_name' => $faker->name,
        'civ' => rand(20, 100),
        'hist' => rand(20, 100),
        'geo' => rand(20, 100),
        'kisw' => rand(20, 100),
        'engl' => rand(20, 100),
        'phy' => rand(20, 100),
        'chem' => rand(20, 100),
        'bio' => rand(20, 100),
        'bmath' => rand(20, 100),
    ];
});
