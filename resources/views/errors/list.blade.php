@if($errors->count())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <span>{{ $error }}</span>
        @endforeach
    </div>
@endif