@extends('layouts.dashboard')

@section('title', 'Charges')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content {{ $charges->count() ? 'table-responsive table-full-width' : '' }}">
                        @if($charges->count())
                            <table class="table table-striped">
                                <thead>
                                <th>ID</th>
                                <th>Upload</th>
                                <th>Candidates</th>
                                <th>Per Candidate</th>
                                <th class="text-right">Amount (TZS)</th>
                                <th>Date</th>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($charges as $charge)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="{{ route('uploads.show', $charge->upload_hash) }}">
                                                {{ $charge->upload_hash }}
                                            </a>
                                        </td>
                                        <td>{{ $charge->candidates }}</td>
                                        <td>{{ $charge->per_candidate }}</td>
                                        <td class="text-right">{{ number_format($charge->total) }}</td>
                                        <td>{{ $charge->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">No Charges</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
