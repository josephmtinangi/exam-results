@extends('layouts.dashboard')

@section('title', 'Wallet')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="card">
                    <div class="content">
                        <div class="row">
                            <div class="col-xs-5">
                                <div class="icon-big icon-success text-center">
                                    <i class="ti-wallet"></i>
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="numbers">
                                    <p>Balance</p>
                                    TZS{{ number_format(Auth::user()->balance) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>        
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Top Up</h4>
                    </div>
                    <div class="content">
                        
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3 col-xs-12">
                                                <label>Payment Method</label>
                                                <select class="form-control">
                                                    <option value="">Airtel Money</option>
                                                    <option value="">Halo Pesa</option>
                                                    <option value="">MPesa</option>
                                                    <option value="">Tigo Pesa</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5 col-xs-12">
                                                <label>Phone number</label>
                                                <input type="text" name="phone" id="phone" placeholder="Phone number" class="form-control border-input">
                                                <p class="help-block text-muted">
                                                    Start with zero
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Pay</button>
                                </form>                                
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
