<ul class="nav">
    <li class="{{ Request::is('home') ? 'active' : '' }}">
        <a href="{{ route('home') }}">
            <i class="ti-panel"></i>
            <p>Home</p>
        </a>
    </li>
    <li class="{{ Request::is('uploads') ? 'active' : '' }}">
        <a href="{{ route('uploads.index') }}">
            <i class="ti-upload"></i>
            <p>Uploads</p>
        </a>
    </li>
    <li class="{{ Request::is('results') ? 'active' : '' }}">
        <a href="{{ route('results.all') }}">
            <i class="ti-files"></i>
            <p>Results</p>
        </a>
    </li>
    <li class="{{ Request::is('wallet') ? 'active' : '' }}">
        <a href="{{ route('wallet') }}">
            <i class="ti-wallet"></i>
            <p>Wallet</p>
        </a>
    </li>
    <li class="{{ Request::is('charges') ? 'active' : '' }}">
        <a href="{{ route('charges.index') }}">
            <i class="ti-exchange-vertical"></i>
            <p>Charges</p>
        </a>
    </li>
    <li class="{{ Request::is('payments') ? 'active' : '' }}">
        <a href="{{ route('payments.index') }}">
            <i class="ti-money"></i>
            <p>Payments</p>
        </a>
    </li>
    <li class="{{ Request::is('settings/profile') ? 'active' : '' }}">
        <a href="{{ route('profile.show') }}">
            <i class="ti-user"></i>
            <p>Profile</p>
        </a>
    </li>    
    <li class="{{ Request::is('settings/account') ? 'active' : '' }}">
        <a href="{{ route('account.show') }}">
            <i class="ti-settings"></i>
            <p>Account</p>
        </a>
    </li>
    <li class="active-pro">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            <i class="ti-export"></i>
            <p>Logout</p>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>        
</ul>
