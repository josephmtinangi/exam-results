@extends('layouts.dashboard')

@section('title', 'Results')

@section('content')

    <div class="container-fluid">
        @isset($hash)
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="card">
                        <div class="content">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="icon-big icon-warning text-center">
                                        <i class="ti-download"></i>
                                    </div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="numbers">
                                        {{-- <p>Exports</p>
                                        NaN --}}
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <hr/>
                                <div class="stats">
                                    <form action="{{ route('results.export', $hash) }}" method="POST">
                                        {{ csrf_field() }}

                                        <button type="submit" class="btn btn-primary"><i class="ti-file"></i> Export</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        @endisset       
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Results</h4>
                        <p class="category">{{ $results->count() }} Candidates</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <th>ID</th>
                            <th>CANDIDATE NAME</th>
                            <th>AGGT</th>
                            <th>DIV</th>
                            <th>DETAILED SUBJECTS</th>
                            <th>Upload Hash</th>
                            </thead>
                            <tbody>
                            	@php $i = 1 @endphp
                            	@foreach($results as $result)
									<tr>
										<td>{{ $i++ }}.</td>
										<td>{{ $result->student_name }}</td>
										<td>{{ $result->aggt }}</td>
										<td>{{ $result->division }}</td>
										<td>
	                                        CIV-{{ $result->civ_grade }}
	                                        HIST-{{ $result->hist_grade }}
	                                        GEO-{{ $result->geo_grade }}
	                                        KISW-{{ $result->kisw_grade }}
	                                        ENGL-{{ $result->engl_grade }}
	                                        PHY-{{ $result->phy_grade }}
	                                        CHEM-{{ $result->chem_grade }}
	                                        BIO-{{ $result->bio_grade }}
	                                        B/MATH-{{ $result->bmath_grade }}
										</td>
                                        <td>
                                            <a href="{{ route('results.index', $result->upload_hash) }}">
                                                {{ $result->upload_hash }}
                                            </a>
                                        </td>
									</tr>
                            	@endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
