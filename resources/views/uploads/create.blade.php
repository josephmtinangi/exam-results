@extends('layouts.dashboard')

@section('title', 'New Upload')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">New Upload</h4>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <form action="{{ route('uploads.store') }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    
                                    <div class="form-group">
                                        <input type="file" name="file" required>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-info btn-fill btn-wd">Upload</button>

                                </form>                                
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <p class="text-muted">
                                    Please upload an excel file containing names of students and their results.
                                    Follow the format below otherwise it will not work
                                </p>
                                <form action="{{ route('download-sample') }}" method="POST">
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-default">Download Sample</button>
                                </form>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection