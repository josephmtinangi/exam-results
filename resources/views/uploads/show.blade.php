@extends('layouts.dashboard')

@section('title', 'Uploads')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="card">
                    <div class="content">
                        @if(Auth::user()->paid($hash))
                            <a href="{{ route('results.index', $hash) }}" class="btn btn-primary">View Results</a>
                        @else
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Candidates</th>
                                            <th>Per Candidate (TZS)</th>
                                            <th>Amount (TZS)</th>
                                            <th>Your Balance (TZS)</th>
                                            @if(Auth::user()->balance < $amount = ($uploads->count() * config('billing.per_candidate')))
                                                <th>Required (TZS)</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $uploads->count() }}</td>
                                            <td>{{ config('billing.per_candidate') }}</td>
                                            <td>
                                                ({{ $uploads->count() }}
                                                x
                                                {{ number_format(config('billing.per_candidate')) }})
                                                =
                                                {{ number_format($amount) }}
                                            </td>
                                            <td> {{ number_format(Auth::user()->balance) }}</td>
                                            @if(Auth::user()->balance < $amount)
                                                <td>
                                                    {{ number_format($amount - Auth::user()->balance) }}
                                                </td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            @if (Auth::user()->balance >= ($uploads->count() * config('billing.per_candidate')))
                                <form action="{{ route('results.store', $hash) }}" method="POST">
                                    {{ csrf_field() }}

                                    <button type="submit" class="btn btn-primary">Process Results</button>
                                </form>
                            @else
                                <button type="button" class="btn btn-primary" disabled>Generate Results</button>
                                <p>You have insufficient balance. Please top up your balance to be able to generate results.</p>
                            @endif
                        @endif

                    </div>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content table-responsive table-full-width">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <th>ID</th>
                            <th>CANDIDATE NAME</th>
                            <th>DETAILED SUBJECTS</th>
                            </thead>
                            <tbody>
                            @php $i = 1 @endphp
                            @foreach($uploads as $upload)
                                <tr>
                                    <td>{{ $i++ }}.</td>
                                    <td>{{ $upload->student_name }}</td>
                                    <td>
                                        CIV-{{ number_format($upload->civ) }}
                                        HIST-{{ number_format($upload->hist) }}
                                        GEO-{{ number_format($upload->geo) }}
                                        KISW-{{ number_format($upload->kisw) }}
                                        ENGL-{{ number_format($upload->engl) }}
                                        PHY-{{ number_format($upload->phy) }}
                                        CHEM-{{ number_format($upload->chem) }}
                                        BIO-{{ number_format($upload->bio) }}
                                        B/MATH-{{ number_format($upload->bmath) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
