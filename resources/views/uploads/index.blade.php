@extends('layouts.dashboard')

@section('title', 'Uploads')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="card">
                    <div class="content">
                        <a href="{{ route('uploads.create') }}" class="btn btn-primary">Upload</a>
                    </div>  
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content {{ $uploads->count() ? 'table-responsive table-full-width' : '' }}">
                        @if($uploads->count())
                            <table class="table table-striped">
                                <thead>
                                <th>ID</th>
                                <th>Upload</th>
                                <th>Candidates</th>
                                <th>Date</th>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($uploads as $upload)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="{{ route('uploads.show', $upload->hash) }}">
                                                {{ $upload->hash }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $upload->candidates($upload->hash) }}
                                        </td>
                                        <td>
                                            {{ $upload->created_at }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">No Upload</p>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
