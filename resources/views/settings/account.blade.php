@extends('layouts.dashboard')

@section('title', 'Profile Settings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Account Settings</h4>
                    </div>
                    <div class="content">

                        @include('errors.list')
                        
                        <form action="{{ route('account.update') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password"
                                               class="form-control border-input" required>
                                    </div> 
                                    <div class="form-group">
                                        <label for="password_confirmation">Password Confirmation</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                               class="form-control border-input" required>
                                    </div>                                   
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info btn-fill btn-wd">Change Password</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
