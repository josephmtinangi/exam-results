@extends('layouts.dashboard')

@section('title', 'Profile Settings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Profile</h4>
                    </div>
                    <div class="content">

                        @include('errors.list')
                        
                        <form action="{{ route('profile.update') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" id="name" value="{{ Auth::user()->name }}"
                                               class="form-control border-input">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" id="email"
                                               value="{{ Auth::user()->email }}"
                                               class="form-control border-input">
                                    </div>                                    
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info btn-fill btn-wd">Update Profile</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection
