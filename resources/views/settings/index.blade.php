@extends('layouts.dashboard')

@section('title', 'Settings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Grades</h4>
                    </div>
                    <div class="content">

                        @include('errors.list')
                        
                        <form action="{{ route('settings.grades.update') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>From (%)</th>
                                            <th>To (%)</th>
                                            <th>Label</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(Auth::user()->settings['grades'] as $grade)
                                            <tr>
                                                <td>
                                                    <select name="from[]" class="form-control border-input" required>
                                                        @foreach(range(100, 0) as $index)
                                                            <option value="{{ $index }}"
                                                            @if ($grade['from'] == $index)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $index }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="to[]" class="form-control border-input" required>
                                                        @foreach(range(100, 0) as $index)
                                                            <option value="{{ $index }}"
                                                            @if ($grade['to'] == $index)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $index }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="label[]" class="form-control border-input" required>
                                                        @foreach($gradesSetup as $gradeSetup)
                                                            <option value="{{ $gradeSetup }}"
                                                            @if($grade['label'] == $gradeSetup)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $gradeSetup }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="value[]" class="form-control border-input" required>
                                                        @foreach(range(count(Auth::user()->settings['grades']), 1) as $index)
                                                            <option value="{{ $index }}"
                                                            @if($grade['value'] == $index)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $index }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            

                            <button type="submit" class="btn btn-info btn-fill btn-wd">Update</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Division</h4>
                    </div>
                    <div class="content">

                        @include('errors.list')
                        
                        <form action="{{ route('settings.aggt.update') }}" method="POST">
                            {{ csrf_field() }}

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>From (Points)</th>
                                            <th>To (Points)</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(Auth::user()->settings['aggt'] as $aggt)
                                            <tr>
                                                <td>
                                                    <select name="from[]" class="form-control border-input" required>
                                                        @foreach(range(35, 7) as $index)
                                                            <option value="{{ $index }}"
                                                            @if ($aggt['from'] == $index)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $index }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="to[]" class="form-control border-input" required>
                                                        @foreach(range(35, 7) as $index)
                                                            <option value="{{ $index }}"
                                                            @if ($aggt['to'] == $index)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $index }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select name="value[]" class="form-control border-input" required>
                                                        @foreach($aggtsSetup as $aggtSetup)
                                                            <option value="{{ $aggtSetup }}"
                                                            @if($aggt['value'] == $aggtSetup)
                                                                selected="selected" 
                                                            @endif
                                                            >{{ $aggtSetup }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                            

                            <button type="submit" class="btn btn-info btn-fill btn-wd">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
