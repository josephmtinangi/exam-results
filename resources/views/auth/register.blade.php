<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="{{ asset('app/assets/paper_img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Register | {{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <link href="{{ asset('app/bootstrap3/css/bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ asset('app/assets/css/ct-paper.css') }}" rel="stylesheet"/>
    <link href="{{ asset('app/assets/css/demo.css') }}" rel="stylesheet"/>
    <link href="{{ asset('app/assets/css/examples.css') }}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

</head>
<body>
<nav class="navbar navbar-ct-transparent navbar-fixed-top" role="navigation-demo" id="register-navbar">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ config('app.url') }}">{{ config('app.name') }}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navigation-example-2">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="{{ route('login') }}" class="btn btn-simple">Login</a>
                </li>
                <li>
                    <a href="#" target="_blank" class="btn btn-simple"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#" target="_blank" class="btn btn-simple"><i class="fa fa-facebook"></i></a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>

<div class="wrapper">
    <div class="register-background">
        <div class="filter-black"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ">
                    <div class="register-card">
                        <h3 class="title">Register</h3>
                        @include('errors.list')
                        <form action="{{ route('register') }}" method="POST" class="register-form">
                            {{ csrf_field() }}

                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email">

                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password">

                            <button type="submit" class="btn btn-danger btn-block">Register</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer register-footer text-center">
            <h6>&copy; {{ date('Y') }}, made with <i class="fa fa-heart heart"></i> by Joseph Mtinangi</h6>
        </div>
    </div>
</div>

</body>

<script src="{{ asset('app/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/assets/js/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('app/bootstrap3/js/bootstrap.js') }}" type="text/javascript"></script>

<!--  Plugins -->
<script src="{{ asset('app/assets/js/ct-paper-checkbox.js') }}"></script>
<script src="{{ asset('app/assets/js/ct-paper-radio.js') }}"></script>
<script src="{{ asset('app/assets/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('app/assets/js/bootstrap-datepicker.js') }}"></script>

<script src="{{ asset('app/assets/js/ct-paper.js') }}"></script>

</html>