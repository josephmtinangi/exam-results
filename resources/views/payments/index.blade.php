@extends('layouts.dashboard')

@section('title', 'Payments')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="content {{ $payments->count() ? 'table-responsive table-full-width' : '' }}">
                        @if($payments->count())
                            <table class="table table-striped">
                                <thead>
                                <th>ID</th>
                                <th class="text-right">AMOUNT (TZS)</th>
                                <th>DATE</th>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td class="text-right">{{ number_format($payment->amount) }}</td>
                                        <td>{{ $payment->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">No Payment</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
