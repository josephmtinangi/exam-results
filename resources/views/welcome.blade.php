@extends('layouts.app')

@section('title', 'Welcome')

@section('content')

    <div class="landing-header" style="background-image: url({{ secure_asset('app/assets/paper_img/cat.jpg') }});">
        <div class="container">
            <div class="motto">
                <h1 class="title-uppercase">Welcome</h1>
                <h3>Process examination results with easy.</h3>
                <br/>
{{--                 <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" class="btn"><i class="fa fa-play"></i>Watch video</a>
                <a class="btn">Download</a> --}}
            </div>
        </div>
    </div>
    <div class="main">
        <div class="section text-center landing-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2>What is {{ config('app.name') }}?</h2>
                        <h5>
                            {{ config('app.name') }} stands for Resulter which is an application that helps you to process examination results. 
                            You upload an excel file with results then for each student we calculate subject grade, points based on the seven best scores and the division.
                            Then you can export the processed results as excel.
                        </h5>{{-- 
                        <br/>
                        <a href="#" class="btn btn-danger btn-fill">See Details</a> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="section section-light-brown landing-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h2>Features</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 column">
                        <h4>Calculate Grades</h4>
                        <p>Grades are calculated using the NECTA standard by default but you can customize the grades in settings.</p>
                        {{-- <a class="btn btn-danger btn-simple" href="#">See more <i class="fa fa-chevron-right"></i></a> --}}
                    </div>
                    <div class="col-md-4 column">
                        <h4>Calculate Aggt</h4>
                        <p>Aggt is calculated using the NECTA standard but you can easily customize them in the settings menu.</p>
                        {{-- <a class="btn btn-danger btn-simple" href="#">See more <i class="fa fa-chevron-right"></i></a> --}}
                    </div>
                    <div class="col-md-4 column">
                        <h4>Calculate Division</h4>
                        <p>For each candidate seven best subjects will be used to calculate the division. Also division criteria is customizable.</p>
                        {{-- <a class="btn btn-danger btn-simple" href="#">See more <i class="fa fa-chevron-right"></i></a> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="section section-dark-blue">
            <div class="container tim-container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h2>Still not convinced?</h2>
                        <p>
                            Register now to get TZS 20, 000/ bonus in wallet and start to process your results.
                        </p>
                    </div>
                    <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 download-area">
                        <a href="{{ route('register') }}" class="btn btn-danger btn-fill btn-block btn-lg">Register Now for Free</a>
                    </div>
                </div>
                <div class="row sharing-area text-center">
                        <h3>Sharing is caring!</h3>
                        <a href="#" class="btn">
                            <i class="fa fa-twitter"></i>
                            Twitter
                        </a>
                        <a href="#" class="btn">
                            <i class="fa fa-facebook-square"></i>
                            Facebook
                        </a>
                </div>
            </div>
        </div>         

        <div class="section section-dark text-center landing-section">
            <div class="container">
                <h2>Steps</h2>
                <div class="col-md-4">
                    <div class="team-player">
                        <h5>Step 1<br/>
                            <small class="text-muted">Upload Results </small>
                        </h5>
                        <p>Upload results in excel format</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-player">
                        <h5>Step 2<br/>
                            <small class="text-muted">Process the results </small>
                        </h5>
                        <p>Click the Process Results button to process the results</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team-player">
                        <h5>Step 3<br/>
                            <small class="text-muted">Export the results </small>
                        </h5>
                        <p>Export the processed results in excel file format</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section-white section-with-space">
            <div class="container tim-container text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h2>Pricing</h2>
                        <p>
                            For each student we process you'll pay us TZS {{ config('billing.per_candidate') }}
                        </p>
                    </div>
                </div>
            </div>
        </div>                       

        <div class="section landing-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-center">Keep in touch?</h2>
                        <form class="contact-form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                    <input class="form-control" placeholder="Name">
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <label>Message</label>
                            <textarea class="form-control" rows="4"
                                      placeholder="Tell us your thoughts and feelings..."></textarea>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <button class="btn btn-danger btn-block btn-lg btn-fill">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
