<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="{{ secure_asset('app/assets/paper_img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <link href="{{ secure_asset('app/bootstrap3/css/bootstrap.css') }}" rel="stylesheet"/>
    <link href="{{ secure_asset('app/assets/css/ct-paper.css') }}" rel="stylesheet"/>
    <link href="{{ secure_asset('app/assets/css/demo.css') }}" rel="stylesheet"/>
    <link href="{{ secure_asset('app/assets/css/examples.css') }}" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="{{ secure_asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

</head>
<body>

@include('partials.app.navbar')

{{-- Announcements --}}

<div class="wrapper">
    
    @yield('content')

</div>

<footer class="footer-demo section-dark">
    <div class="container">
        <nav class="pull-left">
            <ul>

                <li>
                    <a href="{{ config('app.url') }}">
                        {{ config('app.name') }}
                    </a>
                </li>
                <li>
                    <a href="#">
                        Blog
                    </a>
                </li>
                <li>
                    <a href="#">
                        Terms
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright pull-right">
            &copy; {{ date('Y') }}, made with <i class="fa fa-heart heart"></i> by <a href="https://github.com/josephmtinangi">Joseph Mtinangi</a>
        </div>
    </div>
</footer>

</body>

<script src="{{ secure_asset('app/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
<script src="{{ secure_asset('app/assets/js/jquery-ui-1.10.4.custom.min.js') }}" type="text/javascript"></script>

<script src="{{ secure_asset('app/bootstrap3/js/bootstrap.js') }}" type="text/javascript"></script>

<!--  Plugins -->
<script src="{{ secure_asset('app/assets/js/ct-paper-checkbox.js') }}"></script>
<script src="{{ secure_asset('app/assets/js/ct-paper-radio.js') }}"></script>
<script src="{{ secure_asset('app/assets/js/bootstrap-select.js') }}"></script>
<script src="{{ secure_asset('app/assets/js/bootstrap-datepicker.js') }}"></script>

<script src="{{ secure_asset('app/assets/js/ct-paper.js') }}"></script>

</html>