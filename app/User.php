<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'balance', 'settings', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'settings' => 'json',
    ];

    public function uploads()
    {
        return $this->hasMany(Upload::class);
    }

    public function uploadsCount()
    {
        return $this->uploads()->distinct('hash')->get(['hash'])->count();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function charges()
    {
        return $this->hasMany(Charge::class);
    }

    public function paid($hash)
    {
        if ($this->charges()->whereUploadHash($hash)->first()) {
            return true;
        }

        return false;
    }

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function resultsCount()
    {
        return $this->results()->count();
    }

    public function expenditure()
    {
        return $this->charges()->sum('total');
    }
}
