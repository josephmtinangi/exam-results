<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    public function result()
    {
    	return $this->hasOne(Result::class);
    }

    public function candidates($hash)
    {
    	return auth()->user()->uploads()->whereHash($hash)->count();
    }
}
