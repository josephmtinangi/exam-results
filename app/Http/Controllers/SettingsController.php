<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        $gradesSetup = ['A', 'B', 'C', 'D', 'E', 'F'];
        $aggtsSetup = ['I', 'II', 'III', 'IV', '0',];
        return view('settings.index', compact('gradesSetup', 'aggtsSetup'));
    }

    public function updateGrades(Request $request)
    {
        $user = auth()->user();
        $grades = $user->settings['grades'];

        $data = [];

        for($i = 0; $i < count($request->from); $i++)
        {
            $data[] = 
                [
                    'from' => $request->from[$i], 
                    'to' => $request->to[$i], 
                    'label' => $request->label[$i],
                    'value' => $request->value[$i]
                ];
        }

        $user->update([
            'settings' => [
                'grades' => $data,
                'aggt' => $user->settings['aggt'],
            ],
        ]);

        notify()->flash('Grades updated successful.');

        return back();
    }

    public function updateAggt(Request $request)
    {
        $user = auth()->user();
        $aggt = $user->settings['aggt'];

        $data = [];

        for($i = 0; $i < count($request->from); $i++)
        {
            $data[] = 
                ['from' => $request->from[$i], 'to' => $request->to[$i], 'value' => $request->value[$i]];
        }

        $user->update([
            'settings' => [
                'aggt' => $data,
                'grades' => $user->settings['grades'],
            ],
        ]);

        notify()->flash('Aggt updated successful.');

        return back();
    }

    public function showProfile()
    {
    	return view('settings.profile');
    }

    public function updateProfile(Request $request)
    {
    	
    	$data = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
    	]);

    	$user = auth()->user();

    	$user->update($data);

        notify()->flash('Profile updated successful.');

    	return back();
    }

    public function showAccount()
    {
        return view('settings.account');
    }

    public function updateAccount(Request $request)
    {
        
        $data = $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = auth()->user();
        $user->password = bcrypt($data['password']);
        $user->save();

        notify()->flash('Account updated successful.');

        return back();
    }    
}
