<?php

namespace App\Http\Controllers;

use App\Charge;
use App\Result;
use App\Upload;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ResultController extends Controller
{

    public function index($hash = null)
    {
        $upload = $hash ? auth()->user()->uploads()->whereHash($hash)->firstOrFail() : null;

        $results = $upload ? Result::whereUploadHash($upload->hash)->get() : auth()->user()->results;

        return view('results.index', compact('hash', 'results'));
    }

    public function store(Request $request, $hash)
    {
        $uploads = auth()->user()->uploads()->whereHash($hash)->get();

        // Bill the customer
        $user = auth()->user();
        $balance = $user->balance;
        $amount = $uploads->count() * config('billing.per_candidate');

        if ($balance >= $amount) {
            $user->decrement('balance', $amount);
            $user->save();
        } else {
            show_404();
        }

        // Record charge
        $charge = new Charge;
        $charge->user_id = auth()->id();
        $charge->upload_hash = $hash;
        $charge->candidates = $uploads->count();
        $charge->per_candidate = config('billing.per_candidate');
        $charge->total = $amount;
        $charge->save();

        foreach ($uploads as $upload) {
            $result = new Result;
            $result->user_id = auth()->id();
            $result->upload_hash = $upload->hash;
            $result->student_name = $upload->student_name;
            $result->aggt = $this->calculateAggt($upload->id);
            $result->division = $this->calculateDivision($this->calculateAggt($upload->id));
            $result->civ = round($upload->civ);
            $result->civ_grade = $this->calculateGrade($upload->civ);
            $result->hist = round($upload->hist);
            $result->hist_grade = $this->calculateGrade($upload->hist);
            $result->geo = round($upload->geo);
            $result->geo_grade = $this->calculateGrade($upload->geo);
            $result->kisw = round($upload->kisw);
            $result->kisw_grade = $this->calculateGrade($upload->kisw);
            $result->engl = round($upload->engl);
            $result->engl_grade = $this->calculateGrade($upload->engl);
            $result->phy = round($upload->phy);
            $result->phy_grade = $this->calculateGrade($upload->phy);
            $result->chem = round($upload->chem);
            $result->chem_grade = $this->calculateGrade($upload->chem);
            $result->bio = round($upload->bio);
            $result->bio_grade = $this->calculateGrade($upload->bio);
            $result->bmath = round($upload->bmath);
            $result->bmath_grade = $this->calculateGrade($upload->bmath);
            $result->save();
        }

        notify()->flash('Results processed successful.');

        return redirect()->route('results.index', $hash);
    }

    public function calculateAggt($id)
    {
        $upload = Upload::findOrFail($id);

        // Extract scores
        $scores = collect([
            ['value' => $upload->civ, 'grade' => $this->calculateGrade($upload->civ)],
            ['value' => $upload->hist, 'grade' => $this->calculateGrade($upload->hist)],
            ['value' => $upload->geo, 'grade' => $this->calculateGrade($upload->geo)],
            ['value' => $upload->kisw, 'grade' => $this->calculateGrade($upload->kisw)],
            ['value' => $upload->engl, 'grade' => $this->calculateGrade($upload->engl)],
            ['value' => $upload->phy, 'grade' => $this->calculateGrade($upload->phy)],
            ['value' => $upload->chem, 'grade' => $this->calculateGrade($upload->chem)],
            ['value' => $upload->bio, 'grade' => $this->calculateGrade($upload->bio)],
            ['value' => $upload->bmath, 'grade' => $this->calculateGrade($upload->bmath)],
        ]);
        // Sort in descending order
        $sorted = $scores->sortByDesc('value');

        $points = 0;


        // Calculate aggt
        foreach ($sorted->values()->take(7) as $index => $score) {
            if ($score["grade"] === "A") {
                $points += $this->getGradePoint('A');
            } else if ($score["grade"] === "B") {
                $points += $this->getGradePoint('B');
            } else if ($score["grade"] === "C") {
                $points += $this->getGradePoint('C');
            } else if ($score["grade"] === "D") {
                $points += $this->getGradePoint('D');
            } else if ($score["grade"] === "E") {
                $points += $this->getGradePoint('E');
            } else if ($score["grade"] === "F") {
                $points += $this->getGradePoint('F');
            } else {
                $points += 0;
            }
        }

        return $points;
    }

    public function getGradePoint($grade)
    {
        foreach (auth()->user()->settings['grades'] as $gradePoint) {
            if ($gradePoint['label'] == $grade) {
                $point = $gradePoint['value'];
            }
        }

        return $point;
    }

    public function calculateGrade($score)
    {
        $grade = null;

        $score = round($score);

        foreach (auth()->user()->settings['grades'] as $userGrade) {
            if ($score >= $userGrade['from'] && $score <= $userGrade['to']) {
                $grade = $userGrade['label'];
            }
        }

        return $grade;
    }

    public function calculateDivision($points)
    {
        $division = null;
        foreach (auth()->user()->settings['aggt'] as $aggt) {
            if ($points >= $aggt['from'] && $points <= $aggt['to']) {
                $division = $aggt['value'];
            }
        }

        return $division;
    }

    public function export($hash)
    {
        $upload = auth()->user()->uploads()->whereHash($hash)->firstOrFail();

        $results = Result::whereUploadHash($upload->hash)->get();

        $data = [['CANDIDATE NAME', 'AGGT', 'DIVISION', 'CIVICS', 'GRADE', 'HISTORY', 'GRADE', 'GEOGRAPHY', 'GRADE', 'KISWAHILI', 'GRADE', 'ENGLISH', 'GRADE', 'PHYSICS', 'GRADE', 'CHEMISTRY', 'GRADE', 'BIOLOGY', 'GRADE', 'BASIC MATHEMATICS', 'GRADE']];

        foreach ($results as $result) {
            $data[] = [
                $result->student_name,
                $result->aggt,
                $result->division,
                $result->civ,
                $result->civ_grade,
                $result->hist,
                $result->hist_grade,
                $result->geo,
                $result->geo_grade,
                $result->kisw,
                $result->kisw_grade,
                $result->engl,
                $result->engl_grade,
                $result->phy,
                $result->phy_grade,
                $result->chem,
                $result->chem_grade,
                $result->bio,
                $result->bio_grade,
                $result->bmath,
                $result->bmath_grade,
            ];
        }

        Excel::create('Results', function ($excel) use ($data) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}
