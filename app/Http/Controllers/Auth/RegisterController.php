<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'balance' => 20000,
            'settings' => [
                'grades' => [
                    ['from' => 81, 'to' => 100, 'label' => 'A', 'value' => 1],
                    ['from' => 61, 'to' => 80, 'label' => 'B', 'value' => 2],
                    ['from' => 41, 'to' => 60, 'label' => 'C', 'value' => 3],
                    ['from' => 31, 'to' => 40, 'label' => 'D', 'value' => 4],
                    ['from' => 21, 'to' => 30, 'label' => 'E', 'value' => 5],
                    ['from' => 0, 'to' => 20, 'label' => 'F', 'value' => 6],
                ],
                'aggt' => [
                    ['from' => 7, 'to' => 14, 'value' => 'I'],
                    ['from' => 15, 'to' => 21, 'value' => 'II'],
                    ['from' => 22, 'to' => 25, 'value' => 'III'],
                    ['from' => 26, 'to' => 28, 'value' => 'IV'],
                    ['from' => 29, 'to' => 35, 'value' => 'O'],
                ],
            ],
        ]);
    }

    protected function registered(Request $request, $user)
    {
        notify()->flash('Thank you for registering! You have been given TZS 20,000/= as a bonus for registering.');

        return redirect($this->redirectPath());
    }
}
