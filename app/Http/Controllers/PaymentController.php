<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
    	$payments = auth()->user()->payments()->orderBy('created_at', 'DESC')->get();
    	return view('payments.index', compact('payments'));
    }
}
