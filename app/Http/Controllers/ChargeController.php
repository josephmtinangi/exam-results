<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChargeController extends Controller
{
    public function index()
    {
    	$charges = auth()->user()->charges()->orderBy('created_at', 'DESC')->get();
    	return view('charges.index', compact('charges'));
    }
}
