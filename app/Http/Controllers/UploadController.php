<?php

namespace App\Http\Controllers;

use App\Upload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UploadController extends Controller
{
    public function index()
    {
        $uploads = auth()->user()->uploads()->distinct('hash')->orderBy('created_at', 'DESC')->get(['hash', 'created_at']);

        return view('uploads.index', compact('uploads'));
    }

    public function show($hash)
    {
        $uploads = auth()->user()->uploads()->whereHash($hash)->get();

        return view('uploads.show', compact('hash', 'uploads'));
    }

    public function create()
    {
        return view('uploads.create');
    }

    public function store(Request $request)
    {
        $hash = str_random(10);

        // validate

        $rows = Excel::load($request->file('file'), function ($reader) {

        })->get();

        $time = Carbon::now();

        foreach ($rows as $row) {
            $upload = new Upload;
            $upload->user_id = auth()->id();
            $upload->hash = $hash;
            $upload->student_name = $row->student_name;
            $upload->civ = $row->civ;
            $upload->hist = $row->hist;
            $upload->geo = $row->geo;
            $upload->kisw = $row->kisw;
            $upload->engl = $row->engl;
            $upload->phy = $row->phy;
            $upload->chem = $row->chem;
            $upload->bio = $row->bio;
            $upload->bmath = $row->bmath;
            $upload->created_at = $time;
            $upload->updated_at = $time;
            $upload->save();
        }

        notify()->flash('Upload success');

        return redirect()->route('uploads.index');
    }

    public function downloadSample()
    {
        return response()->download(public_path('/samples/sample-students.xlsx'));
    }
}
