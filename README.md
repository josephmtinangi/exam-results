# Exam Results

## Database Design

### Models

User
 - name
 - email
 - password

Uploads
 - user_id
 - name
 - subjects

Results
 - upload_id
 - user_id
 - name
 - subjects

### Relationships

- User has many Uploads

- Upload belongs to User

- User has many Results

- Result belongs to User

- Upload has One Result

- Result belongs to Upload
